import React from 'react'

export default class Room extends React.Component {
    constructor(props) {
		super(props)
    }
    
    render() {
        const power = this.props.power;

        return (
            <div className='room' style={power === 'on' ? {backgroundColor: 'rgb(230, 230, 230'} : {backgroundColor: 'rgb(77, 77, 77)'}}>
                <div className="light">
                    <div 
                        className="bulb"
                        style={power === 'on' ? {backgroundColor: 'rgb(255, 255, 255)'} : {backgroundColor: 'rgb(20, 20, 20)'}}>
                    </div>
                </div>
                <p style={power === 'on' ? {color: 'black'} : {color: 'white'}}>Room</p>
            </div>
        )
    }
}