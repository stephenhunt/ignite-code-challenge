import React from 'react'

export default class Switch extends React.Component {
    constructor(props) {
        super(props);
        this.powerSwitch = this.powerSwitch.bind(this);
    }

    powerSwitch(e) {
        console.log(e);
        this.props.onPowerChange((e.target.value == 'on' ? 'off' : 'on'));
    }
    
    render() {
        const power = this.props.power;

        return (
            <div className='switchplate'>
                <label className='switch'>
                    <input type='checkbox' onChange={this.powerSwitch} id='lightToggle' value={power} />
                    <span className='toggle'></span>
                </label>
                <p>Switch</p>
            </div>
        )
    }
}