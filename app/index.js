import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Room from './components/Room'
import Switch from './components/Switch'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.handlePowerSwitch = this.handlePowerSwitch.bind(this);

		this.state = {
			light: 'off'
		}
    }

    handlePowerSwitch(power) {
        this.setState({ light: power });
    }
    
    render() {
        const powerState = this.state.light;

        return (
            <div className='container'>
                <Room 
                    power={powerState} />
                <Switch 
                    power={powerState}
                    onPowerChange={this.handlePowerSwitch} />
            </div>
        )
    }
}

ReactDOM.render (
    <App />,
    document.getElementById('app')
)